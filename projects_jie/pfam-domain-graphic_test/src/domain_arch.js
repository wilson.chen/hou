import React, { Component } from "react";
import ReactDOM from "react-dom";
import PfamGraphic from "./static/domain-arch.js";
import "./static/domain_arch.css";

import "./static/html2canvas.js";
import  "./static/blasterjs.js";

class Domain_arch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curids: "",
    };
  }

  componentDidMount() {
    //this.renderDomainArch();
    this.renderblast();
  }

  renderDomainArch() {
    const parent2 = document.createElement("span");
    parent2.setAttribute("id", Math.floor(Math.random() * 100).toString());
    document
      .getElementById(`main_domain_cont_${this.props.tag}`)
      .appendChild(parent2);
    const data2 = {
      length: "950",
      regions: [
        {
          modelStart: "5",
          modelEnd: "292",
          colour: "#2dcf00",
          endStyle: "jagged",
          startStyle: "jagged",
          display: true,
          end: "361",
          aliEnd: "361",
          href: "/family/PF00082",
          text: "Peptidase_S8",
          modelLength: "307",
          metadata: {
            scoreName: "e-value",
            score: "1.3e-38",
            description: "Subtilase family",
            accession: "PF00082",
            end: "587",
            database: "pfam",
            aliEnd: "573",
            identifier: "Peptidase_S8",
            type: "Domain",
            aliStart: "163",
            start: "159",
          },
          type: "pfama",
          aliStart: "163",
          start: "159",
        },
        {
          modelStart: "5",
          modelEnd: "292",
          colour: "#2dcf00",
          endStyle: "jagged",
          startStyle: "jagged",
          display: true,
          end: "587",
          aliEnd: "573",
          href: "/family/PF00082",
          text: "Peptidase_S8",
          modelLength: "307",
          metadata: {
            scoreName: "e-value",
            score: "1.3e-38",
            description: "Subtilase family",
            accession: "PF00082",
            end: "587",
            database: "pfam",
            aliEnd: "573",
            identifier: "Peptidase_S8",
            type: "Domain",
            aliStart: "163",
            start: "159",
          },
          type: "pfama",
          aliStart: "470",
          start: "470",
        },
      ],
      metadata: {
        database: "uniprot",
        identifier: "Q560V8_CRYNE",
        organism: "Cryptococcus neoformans (Filobasidiella neoformans)",
        description: "Putative uncharacterized protein",
        taxid: "5207",
        accession: "Q560V8",
      },
    };
    var c = new PfamGraphic(parent2, data2);
    // this.pfamG.initialize
    return c.render(parent2, data2);
  }
  
  renderblast(){
    const parent1 = document.createElement("div");
    parent1.setAttribute("id", "blast-multiple-alignments");
    document
      .getElementById(`main_domain_cont_${this.props.tag}`)
      .appendChild(parent1);

    const parent2 = document.createElement("div");
    parent2.setAttribute("id", "blast-alignments-table");
    document
      .getElementById(`main_domain_cont_${this.props.tag}`)
      .appendChild(parent2);
      
    const parent3 = document.createElement("div");
    parent3.setAttribute("id", "blast-single-alignment");
    document
      .getElementById(`main_domain_cont_${this.props.tag}`)
      .appendChild(parent2);
      
        var blasterjs = require("/Users/jay/Documents/slu/Colloboration/Dapeng_lab/toxin_databases/webserver/hou/projects_jie/pfam-domain-graphic_test/src/biojs-vis-blasterjs");
        var instance  = new blasterjs({
            input: "blastinput",
            multipleAlignments: "blast-multiple-alignments",
            alignmentsTable: "blast-alignments-table",
            singleAlignment: "blast-single-alignment"
        });    
  }

  render() {
    console.log("render() lifecycle");

    return <div id={`main_domain_cont_${this.props.tag}`}></div>;
    
  }
}

export default Domain_arch;
